module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "~> 3"

  name = "${var.name}-${var.environment}"
  cidr = "20.10.0.0/16" # 10.0.0.0/8 is reserved for EC2-Classic

  azs                 = data.aws_availability_zones.available.names 
  private_subnets     = ["20.10.1.0/24", "20.10.2.0/24", "20.10.3.0/24"]
  public_subnets      = ["20.10.11.0/24", "20.10.12.0/24", "20.10.13.0/24"]
  database_subnets    = ["20.10.21.0/24", "20.10.22.0/24", "20.10.23.0/24"]

  private_subnet_tags = { "name": "${var.private_subnet_suffix}-${var.name}-${var.environment}" }
  public_subnet_tags = { "name": "${var.public_subnet_suffix}-${var.name}-${var.environment}" }
  database_subnet_tags = { "name": "${var.database_subnet_suffix}-${var.name}-${var.environment}" }

  create_database_subnet_group = true

  enable_nat_gateway = true
  single_nat_gateway = true

  enable_dhcp_options              = false

  # Default security group - ingress/egress rules cleared to deny all
  manage_default_security_group  = true
  default_security_group_ingress = []
  default_security_group_egress  = []

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60

  tags = local.default_tags
}
