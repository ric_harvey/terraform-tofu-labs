![Terraform/OpenTofu Labs](./img/terraform-tofu-labs.png)

## Terraform and OpenTofu Labs for AWS

Welcome to the Terraform/Tofu labs for AWS. This set of labs aims to teach you the basics of terraform/tofu, to get you started on your journey. It focuses on deploying resources to AWS (Amazon Web Services) and keeps costs at a minimum.

> [!Note]
> Don't forget to do the clean up steps after the labs to shut down the infrastructure. 

#### Requirements
- You'll need an AWS account from [https://aws.amazon.com](https://aws.amazon.com)
- If you want to do lab 4 you'll need an account at [https://gitlab.com](https://gitlab.com)

It's recommended that you are familiar with the linux command line and bash, however you can just copy and past from the tutorials

### Labs

- [Lab 1](./1-getting-started/README.md) - Will get you setup with the tooling and show you the basic commands
- [Lab 2](./2-simple-example/README.md) - Will introduce you to variables
- [Lab 3](./3-remote-states/README.md) - Will help you create and setup remote states using dynamoDB and S3
- [Lab 4](./4-gitlab-ci/README.md) - Will walk you through setting up a Gitlab CI/CD pipeline with Terraform (tofu coming soon)

### Links

- [Terraform](https://terraform.io)
- [OpenTofu](https://opentofu.org/)

### Trouble Shooting

#### No Space left on device
If you experience and issue like below on your CloudShell you'll need to delete any **unused** ```terraform.tfstate``` and ```terraform.tfstate.backup``` in previous directories. You may also want to consider removing ```.terraform``` folders from each code directory as this will free up the most space.

![no space left on disk](./img/no-space.png)

> [!Note]
> Make sure you have run terraform destroy before deleting on-disk state files, otherwise you'll need to manually delete resources in the AWS console to avoid charges!

---

##### Follow me for more guides

[<img src="./img/mastodon.png">](https://awscommunity.social/@Ric) [<img src="./img/gitlab.png">](https://gitlab.com/ric_harvey) [<img src="./img/linkedin.png">](https://www.linkedin.com/in/richarvey/)

<div align=center>
[The Terraform Tofu Labs](https://gitlab.com/ric_harvey/terraform-tofu-labs) by [Ric Harvey](https://awscommunity.social/@Ric) is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1")

