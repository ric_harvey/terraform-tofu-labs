## Introduction

First off this lab is entirely optional and also not for the faint hearted. Gitlab CI is currently introducing components to replace the current templates and openTofu will be updated to that as soon as they are supported in self managed runners. If you're not comfortable experimenting and cleaning things up in AWS manually **don't do this lab**. For this lab I recommend using the terraform template, I'll update this once the tofu templates are ready.

First things first lets get some things setup. Make sure you have a free account on [https://gitlab.com](https://gitlab.com)

### Get AWS Credentials for Gitlab

Log into the AWS console and head to the IAM service. In here you need to create a new user called gitlab.

![Generate an AWS user](./img/aws-1.png)

Hit next and continue. On the next screen click attach policy and choose Administrator Access (this is because you sometimes need to use terraform/tofu to create IAM policies)

![Generate an AWS user](./img/aws-2.png)

Now click next until the user is created. You can now click that user in the IAM console and click on the security tab. Here you'll need to click on ```create access key```

![Generate an AWS user](./img/aws-3.png)

Now you can select the top option ```Command Line Interface``` and check the accept box at the bottom of the page then hit next.

![Generate an AWS user](./img/aws-4.png)

Enter gitlab access for the optional field.

![Generate an AWS user](./img/aws-5.png)

Now click ```Download CSV``` and save this file for getting your access keys from later, you'll need to input these into Gitlab.

![Generate an AWS user](./img/aws-6.png)

### Set up Gitlab CI/CD

#### First steps
1. Login to Gitlab and create a new private repo
2. Clone that repo either into CloudShell or your local machine
3. Copy the files from this directories code folder into your new cloned repo
4. Edit ```versions.tf``` to match your bucket and table created in lab 3
5. Run ```git add .```
6. Run ```git commit -a -m "initial commit```
7. Run ```git push```

#### Set up variables for CI/CD
1. Login to Gitlab
2. Open your project repo
3. Click on Settings > CI/CD

![Settings in Gitlab](./img/gitlab-vars-1.png)

4. Expand the Variables section
5. Add the following variables as masked and expanded, untick protected. Create Variables for:
- AWS_ACCESS_KEY_ID (use the value from your downloaded CSV)
- AWS_SECRET_ACCESS_KEY (use the value from your downloaded CSV)
- AWS_DEFAULT_REGION (set to eu-west-1)

![Setting variables in Gitlab](./img/variable-full.png)

#### Add a ci/cd template

1. Login to Gitlab
2. Go to your project
3. Go to ```Build``` in the left hand menu and then ```Pipeline Editor```

![Pipeline Editor](./img/pipeline.png)

4. Click Configure Pipeline
5. Click Browse templates or copy the below and paste into your pipeline:

```yaml
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml

include:
  - template: Terraform/Base.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml

stages:
  - validate
  - test
  - build
  - deploy
  - cleanup

fmt:
  extends: .terraform:fmt
  needs: []

validate:
  extends: .terraform:validate
  needs: []

build:
  extends: .terraform:build
  environment:
    name: demo
    action: prepare

deploy:
  extends: .terraform:deploy
  dependencies:
    - build
  environment:
    name: demo
    action: start
```

6. Click commit and your build _should_ begin
7. You'll need to manually click deploy in the pipeline view

![Build Pipeline](./img/build.png)

#### Clean Up

You should be able to run the following from your local directory as the state file is configured to be remote.

```bash
tofu destroy
```

---

##### Follow me for more guides

[<img src="./img/mastodon.png">](https://awscommunity.social/@Ric) [<img src="./img/gitlab.png">](https://gitlab.com/ric_harvey) [<img src="./img/linkedin.png">](https://www.linkedin.com/in/richarvey/)

<div align=center>
[The Terraform Tofu Labs](https://gitlab.com/ric_harvey/terraform-tofu-labs) by [Ric Harvey](https://awscommunity.social/@Ric) is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1")

