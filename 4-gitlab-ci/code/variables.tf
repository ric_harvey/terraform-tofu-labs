variable "name" {
  description = "Name of our Application"
  type        = string
  default     = "lab-1-app"
}

variable "environment" {
  description = "The deployment environment"
  type        = string
  default     = "dev"
}

variable "private_subnet_suffix" {
  description = "Suffix to append to private subnets name"
  type        = string
  default     = "private-"
}

variable "public_subnet_suffix" {
  description = "Suffix to append to public subnets name"
  type        = string
  default     = "public-"
}

variable "database_subnet_suffix" {
  description = "Suffix to append to database subnets name"
  type        = string
  default     = "rds-"
}

variable "additional_tags" {
  description = "Additional default resource tags"
  type        = map(string)
  default     = {}
}

variable "instance_size" {
  description = "Size of the instance to run"
  type        = string
  default     = "t4g.nano"
}

variable "ami_id" {
  description = "Instance Amazon Machine Image to run"
  type        = string
  default     = "ami-029b91ed285a24a90"
}

