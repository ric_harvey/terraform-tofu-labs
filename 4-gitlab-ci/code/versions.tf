terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.66.1"
    }
  }

    backend "s3" {
    bucket         = "demo-tofu-bucket"
    key            = "terraform-tofu-lab/terraform.state"
    region         = "eu-west-1"
    acl            = "bucket-owner-full-control"
    dynamodb_table = "demo-tofu-table"
  }

}
