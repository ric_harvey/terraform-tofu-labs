locals {
  default_tags = merge(
    var.additional_tags,
    {
      Owner       = var.name
      Environment = var.environment
      ManagedBy   = "tofu/terraform"
  })
}
