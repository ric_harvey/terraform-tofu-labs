## Introduction

In this first lab we are going to do the basics. We'll:

 - Log into AWS
 - Use cloudshell (as this has all your credentials set up already)
 - Get your environment ready to run terraform/tofu
 - Run a simple example

### Log into AWS

First of all head to [https://aws.amazon.com](https://aws.amazon.com) and sign into your console. The next thing to do is check that you are working inthe correct region. For this lab everything is coded for eu-west-1 (Ireland) so make sure you select the correct region at the top right hand of the page.

![eu-west-1 Ireland Region](./img/eu-west-1.png)

Once in this region we are setup for the following labs.

> [!NOTE]
> We'll be creating infrastructure throughout and AWS does charge for your actual usage 

### Open CloudShell

For these labs we are going to use the AWS supplied CloudShell, now you can do this from your own machine, however, you'll need to setup the [AWS CLI tool](https://aws.amazon.com/cli/) and AWS credentials which is beyond the scope of these labs. The nice thing about CloudShell is that it's already configured with your credentials for accessing AWS resources. You also launch this by heading up  to the main bar in the AWS console and clicking the icon highlighted below:

![Launch CloudShell](./img/cloudshell.png)

Once launched you can hit the arrow to break the shell out into it's own browser tab. I personally find it easier to work this way. It's also good to know there is no charge to run CloudShell. It'll look a little like this when opened:

![CloudShell in it's own browser window](./img/cloudshell2.png)

Right lets get you setup to run terraform or openTofu! You only need to do one of these so choose your tool of choice. I lean toward using tofu as it's fully open source, but if you want some of the newer features in terraform 1.8.0 and higher the guide is here for you also.

> [!Note]
> Periodically you CloudShell is rebuilt and anything thats not stored in your home directory will be deleted, for this reason the method's below store the tooling in your home directory.

#### Install Terraform

First we are going to install a tool that make's it easy to install the binaries for Terraform. Copy and Paste the below into your CloudShell:

```bash
git clone https://github.com/tfutils/tfenv.git ~/.tfenv
mkdir ~/bin
ln -s ~/.tfenv/bin/* ~/bin/
```

Find the latest version and install it (at the time of writing it was **1.8.0**):

```bash
tfenv list-remote
tfenv install 1.8.0
tfenv use 1.8.0
```

Now test it's all working:

```bash
terraform --version
```

The output should look likt e following screenshot:

![Terraform output of the version flag](./img/terraform-working.png)

#### Install Tofu

If like me you prefer to use open source tools this is how to make tofu persist in CloudShell, Copy and Paste the following into your terminal:

```bash
# Download the installer script:
curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh
# Alternatively: wget --secure-protocol=TLSv1_2 --https-only https://get.opentofu.org/install-opentofu.sh -O install-opentofu.sh

# Give it execution permissions:
chmod +x install-opentofu.sh

# Please inspect the downloaded script

# Run the installer:
./install-opentofu.sh --install-method rpm

# Remove the installer:
rm install-opentofu.sh

# Make Tofu remain in your path after reboots
mkdir ~/bin
sudo mv /usr/bin/tofu ~/bin
```

Now test it's all working:

```
tofu --version
```

The output should look likt e following screenshot:

![Tofu output of the version flag](./img/tofu-working.png)

Now your tooling is installed and should survice you reconnecting to CloudShell.

### Test Deployment

Before we deploy to AWS we need to pull the code down from [GitLab](https://gitlab.com) onto your CloudShell. To do this run the follwoing commands:

```bash
git clone https://gitlab.com/ric_harvey/terraform-tofu-labs.git
cd terraform-tofu-labs
```
The output will look like the following:

![Git Clone the examples](./img/git-clone.png)

Now lets run some code.

> [!Note]
> You can replace the commands for ```terraform``` from ```tofu``` if you are running that version

#### init

First lets initialise our terraform/tofu environment:

```bash
cd 1-getting-started/code/
tofu init
```

![tofu init output on screen](./img/tofu-init.png)

This has installed all the providers and modules you need for running your code, basically it's downloaded any dependancies.

#### plan

Now we are going to run ```tofu plan```, this will test that your code will actually run and compare the code to any previous deployed resources saved in a state file (more on this later).

```bash
tofu plan
```

This command will generate a lot of output, if you get any RED text you have an issue and you'll need to debug. The code in the repository should be fine however, if not please open an issue.

![tofu plan output](./img/tofu-plan.png)

#### apply

Now lets apply this IaC and deploy the following:

- A VPC
- 9 Subnets
	- 3 Public
	- 3 Private
	- 3 Database
- An EC2 instance with a Security Group
	- Allows TCP ports 22,80 and 443 ingress
	- Allows all egress

Lets run the following code:

```bash
tofu apply
```

![tofu apply output and prompt](./img/tofu-apply-1.png)

At the prompt you need to type **yes** in order to actually deploy the infrastructure, anything else will result in the program terminating. Once complete you should see the following output:

![tofu apply output after sucessfully deploying](./img/tofu-apply-2.png)

The Outputs seen on the screen are controlled by the ```outputs.tf``` file in the repo. If you browse in the AWS console to **EC2 > Instances** you should see that there is indeed a new EC2 instance running in your account.

![Running EC2 instance in the aws console](./img/deployed-ec2.png)

#### destroy

Right time is money as they say, lets shut this down and stop spending it. Luckily in tofu/terraform this is super easy to do and we can run the following command. Tofu will reference the ```.tfstate``` file and see what it needs to do to shut everything down.

```bash
tofu destroy
```

This command can take a while to run, be patient and it'll do it's job. You'll get prompted along the way yo confirm you really want to delete everything, make sure to answer **yes** again.

![tofu destroy prompt to continue](./img/tofu-destroy-1.png)

Once complete, you'll get some confirmation like the picture below:

![tofu destroy completed](./img/tofu-destroy-2.png)

> [!Note]
> Don't worry about the warning you'll be ok with the acl not being deleted, it won't cost anything

### Recap

What we've learnt during this exercise:

- How to install terraform/tofu in cloud shell
- terraform/tofu basic commands
	- init
	- plan
	- apply
	- destroy

Now lets move onto the next lab to learn how to customise this and build upon it.

---

##### Follow me for more guides

[<img src="./img/mastodon.png">](https://awscommunity.social/@Ric) [<img src="./img/gitlab.png">](https://gitlab.com/ric_harvey) [<img src="./img/linkedin.png">](https://www.linkedin.com/in/richarvey/)

<div align=center>
[The Terraform Tofu Labs](https://gitlab.com/ric_harvey/terraform-tofu-labs) by [Ric Harvey](https://awscommunity.social/@Ric) is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1")

